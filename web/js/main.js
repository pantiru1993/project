$(document).ready(function($){
    //if you change this breakpoint in the style.css file (or _layout.scss if you use SASS), don't forget to update this value as well
    var MqL = 1170;
    //move nav element position according to window width
    moveNavigation();
    $(window).on('resize', function(){
            (!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);
    });

    //mobile - open lateral menu clicking on the menu icon
    $('.cd-nav-trigger').on('click', function(event){
            event.preventDefault();
            if( $('.cd-main-content').hasClass('nav-is-visible') ) {
                    closeNav();
                    $('.cd-overlay').removeClass('is-visible');
            } else {
                    $(this).addClass('nav-is-visible');
                    $('.cd-main-header').addClass('nav-is-visible');
                    $('.cd-main-content').addClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                            $('body').addClass('overflow-hidden');
                    });
                    toggleSearch('close');
                    $('.cd-overlay').addClass('is-visible');
            }
    });

    //open search form
    $('.cd-search-trigger').on('click', function(event){
            event.preventDefault();
            toggleSearch();
            closeNav();
    });

    //close search form on click on something else
    $(document).click(function(e) {
        if ($(e.target).is('.search-wrapper *')) {
            return;
        }
        toggleSearch('close');
     });

    //submenu items - go back link
    $('.go-back').on('click', function(){
            $(this).parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out');
    });

    function closeNav() {
        $('.cd-nav-trigger').removeClass('nav-is-visible');
        $('.cd-main-header').removeClass('nav-is-visible');
        $('.cd-primary-nav').removeClass('nav-is-visible');
        $('.has-children ul').addClass('is-hidden');
        $('.has-children a').removeClass('selected');
        $('.moves-out').removeClass('moves-out');
        $('.cd-main-content').removeClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                $('body').removeClass('overflow-hidden');
        });
    }

    function toggleSearch(type) {
        if(type == "close") {
                //close serach 
                $('.cd-search').removeClass('is-visible');
                $('.cd-search-trigger').removeClass('search-is-visible');
                $('.cd-overlay').removeClass('search-is-visible');
        } else {
                //toggle search visibility
                $('.cd-search').toggleClass('is-visible');
                $('.cd-search-trigger').toggleClass('search-is-visible');
                $('.cd-overlay').toggleClass('search-is-visible');
                //if($(window).width() > MqL && $('.cd-search').hasClass('is-visible')) $('.cd-search').find('input[type="search"]').focus();
                if($(window).width() > MqL && $('.cd-search').hasClass('is-visible')) $('.cd-search').find('.form-control.tt-input').focus();
                ($('.cd-search').hasClass('is-visible')) ? $('.cd-overlay').addClass('is-visible') : $('.cd-overlay').removeClass('is-visible') ;
        }
    }

    function checkWindowWidth() {
        //check window width (scrollbar included)
        var e = window, 
        a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    if ( e[ a+'Width' ] >= MqL ) {
                    return true;
            } else {
                    return false;
            }
    }

    function moveNavigation(){
            var navigation = $('.cd-nav');
            var desktop = checkWindowWidth();
        if ( desktop ) {
            navigation.detach();
            navigation.insertBefore('.cd-header-buttons');
        } else {
            navigation.detach();
            navigation.insertAfter('.cd-main-content');
        }
    };

//    $(".dropdown").hover(
//        function () {
//            $('.dropdown-menu', this).stop(true, true).slideDown("fast");
//            $(this).addClass('open');
//            //$(this).closest('.dropdown-toggle').attr('aria-expanded', true);
//        },
//        function () {
//            $('.dropdown-menu', this).stop(true, true).slideUp("fast");
//            $(this).removeClass('open');
//            //$(this).closest('.dropdown-toggle').attr('aria-expanded', true);
//        }
//    );
//
//    $("document").ready(function() {
//        $("#video").simplePlayer();
//    });
//    $("document").ready(function() {
//        $("#video1").simplePlayer();
//    });
//    $("document").ready(function() {
//        $("#video2").simplePlayer();
//    });
//    $("document").ready(function() {
//        $("#video3").simplePlayer();
//    });
//
//    $('.w3_play_icon,.play-icon-1,.w3_play_icon2').magnificPopup({
//        type: 'inline',
//        fixedContentPos: false,
//        fixedBgPos: true,
//        overflowY: 'auto',
//        closeBtnInside: true,
//        preloader: false,
//        midClick: true,
//        removalDelay: 300,
//        mainClass: 'my-mfp-zoom-in'
//    });
    
    if ($('#horizontal-tab').length) {
        $('#horizontal-tab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
            }
        });
    }
    
    if($('#verticalTab').length) {
        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    }
    
    if($(".owl-carousel").length) {
        $(".owl-carousel").owlCarousel({

            autoPlay: 3000, //Set AutoPlay to 3 seconds
            navigation :true,

            items : 5,
            itemsDesktop : [640,4],
            itemsDesktopSmall : [414,3],
            navigationText: ['<i class="fas fa-angle-left"></i>','<i class="fas fa-angle-right"></i>']

        });
    }

    $(".scroll").click(function(event){		
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top},900);
    });

    /*
    var defaults = {
        containerID: 'toTop', // fading element id
        containerHoverID: 'toTopHover', // fading element hover id
        scrollSpeed: 1200,
        easingType: 'linear' 
    };
    */

    $().UItoTop({ easingType: 'easeOutQuart' });
    
    if($('.accordion').length) {
        $('.accordion').accordion({
            "transitionSpeed": 400
        });
    }    
    
    if($('#slinky-menu').length) {
        const slinky = $('#slinky-menu').slinky({
                "title" : true
            });
    }

});