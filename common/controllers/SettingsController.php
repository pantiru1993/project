<?php

namespace app\common\controllers;

use dektrium\user\controllers\SettingsController as BaseSettingsController;

/**
 * Default controller for the `settings` module
 */
class SettingsController extends BaseSettingsController
{   
    public function beforeAction($action)
    {
       \Yii::$app->user->identity->isAdmin ?
           $this->layout = '@app/modules/admin/views/layouts/main.php' :
           $this->layout = '@app/modules/project/views/layouts/main.php';
       
       return parent::beforeAction($action);
    }
}