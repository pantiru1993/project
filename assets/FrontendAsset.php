<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrontendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/popup-box.css',
        'css/all.min.css', //font-awesome
        'css/zoomslider.css',
        'css/style.css',        
        '//fonts.googleapis.com/css?family=Dosis%3A400%2C700%2C500%7CNunito%3A300%2C400%2C600&subset=latin%2Clatin-ext&ver=2014-12-20',
        'css/jquery.accordion.css',
        'css/slinky.min.css',

    ];
    public $js = [
        'js/bootstrap.js',
        'js/modernizr-2.6.2.min.js',
        'js/jquery.zoomslider.min.js',
//        'js/simplePlayer.js',
//        'js/jquery.magnific-popup.js',
        'js/move-top.js',
        'js/easing.js',        
        'js/jquery.accordion.js',
        'js/slinky.min.js',
        'js/main.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
