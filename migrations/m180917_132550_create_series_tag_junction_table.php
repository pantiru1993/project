<?php

use yii\db\Migration;

/**
 * Handles the creation of table `series_tag_junction`.
 */
class m180917_132550_create_series_tag_junction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // MySql table options
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        
        $this->createTable(
            '{{%series_tag_assn}}',
            [
                'series_id' => $this->integer()->notNull(),
                'tag_id' => $this->integer()->notNull(),
            ],
            $tableOptions
        );
        
        $this->addPrimaryKey('', '{{%series_tag_assn}}', ['series_id', 'tag_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%series_tag_assn}}');
    }
}
