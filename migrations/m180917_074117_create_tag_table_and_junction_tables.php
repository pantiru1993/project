<?php

use yii\db\Migration;

/**
 * Class m180917_074117_create_tag_table_and_junction_tables
 */
class m180917_074117_create_tag_table_and_junction_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // MySql table options
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        // table series
        $this->createTable(
            '{{%tag}}',
            [
                'id' => $this->primaryKey(),
                'name' => $this->string(255)->notNull(),
                'frequency' => $this->integer()->notNull()->defaultValue(0)
            ],
            $tableOptions
        );
        
        $this->createTable(
            '{{%movie_tag_assn}}',
            [
                'movie_id' => $this->integer()->notNull(),
                'tag_id' => $this->integer()->notNull(),
            ],
            $tableOptions
        );
        
        $this->addPrimaryKey('', '{{%movie_tag_assn}}', ['movie_id', 'tag_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%tag}}');
        $this->dropTable('{{%movie_tag_assn}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180917_074117_create_tag_table_and_junction_tables cannot be reverted.\n";

        return false;
    }
    */
}
