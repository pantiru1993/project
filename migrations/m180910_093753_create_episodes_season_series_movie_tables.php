<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180910_093753_create_posts_and_categories_tables
 */
class m180910_093753_create_episodes_season_series_movie_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // MySql table options
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        // table series
        $this->createTable(
            '{{%series}}',
            [
                'id' => $this->primaryKey(),
                'title' => $this->string(255)->notNull(),
                'brief' => $this->text(),
                'slug' => $this->string(128)->notNull(),
                'year' => $this->integer()->notNull(),
                'trailer' => $this->string(255),
                'large_banner' => $this->string(255),
                'small_banner' => $this->string(255),
                'imdb_rating' => $this->decimal(3, 1),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
                'hits_count' => $this->integer()->defaultValue(0)
            ],
            $tableOptions
        );
        
        // table season
        $this->createTable(
            '{{%season}}',
            [
                'id' => $this->primaryKey(),
                'series_id' => $this->integer()->notNull(),
                'season_number' => $this->integer()->notNull(),
                'title' => $this->string(255)->notNull(),
                'year' => $this->integer()->notNull(),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
                'hits_count' => $this->integer()->defaultValue(0)
            ],
            $tableOptions
        );
        
        // table episode
        $this->createTable(
            '{{%episode}}',
            [
                'id' => $this->primaryKey(),
                'season_id' => $this->integer()->notNull(),
                'episode_number' => $this->integer()->notNull(),
                'title' => $this->string(255)->notNull(),
                'brief' => $this->text(),
                'slug' => $this->string(128)->notNull(),
                'year' => $this->integer()->notNull(),
                'trailer' => $this->string(255),
                'server_1' => $this->string(255),
                'server_2' => $this->string(255),
                'server_3' => $this->string(255),
                'large_banner' => $this->string(255),
                'small_banner' => $this->string(255),
                'imdb_rating' => $this->decimal(3, 1),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
                'hits_count' => $this->integer()->defaultValue(0)
            ],
            $tableOptions
        );
        
        // table episode
        $this->createTable(
            '{{%movie}}',
            [
                'id' => $this->primaryKey(),
                'title' => $this->string(255)->notNull(),
                'brief' => $this->text(),
                'slug' => $this->string(128)->notNull(),
                'year' => $this->integer()->notNull(),
                'trailer' => $this->string(255),
                'server_1' => $this->string(255),
                'server_2' => $this->string(255),
                'server_3' => $this->string(255),
                'large_banner' => $this->string(255),
                'small_banner' => $this->string(255),
                'imdb_rating' => $this->decimal(3, 1),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
                'hits_count' => $this->integer()->defaultValue(0)
            ],
            $tableOptions
        );
        
        // Indexes
        $this->createIndex('series_id', '{{%season}}', 'series_id');
        $this->createIndex('season_id', '{{%episode}}', 'season_id');
        $this->createIndex('created_at', '{{%episode}}', 'created_at');
        // Foreign Keys
        $this->addForeignKey('FK_series_id_series', '{{%season}}', 'series_id', '{{%series}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_season_id_season', '{{%episode}}', 'season_id', '{{%season}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%episode}}');
        $this->dropTable('{{%series}}');
        $this->dropTable('{{%season}}');
        $this->dropTable('{{%movie}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180910_093753_create_posts_and_categories_tables cannot be reverted.\n";

        return false;
    }
    */
}
