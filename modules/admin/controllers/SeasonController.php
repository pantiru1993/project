<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Season;
use app\modules\admin\models\SeasonSearch;
use app\modules\admin\models\Series;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * SeasonController implements the CRUD actions for Season model.
 */
class SeasonController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Season models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SeasonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Season model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Season model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Season();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Season <strong>$model->title</strong> was created succesfully.");
            return $this->redirect(['season/index']);
        }

        return $this->render('create', [
            'model' => $model,
            'series' => yii\helpers\ArrayHelper::map(Series::find()->all(),'id','title')
        ]);
    }

    /**
     * Updates an existing Season model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Season <strong>$model->title</strong> was updated succesfully.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'series' => yii\helpers\ArrayHelper::map(Series::find()->all(),'id','title')
        ]);
    }

    /**
     * Deletes an existing Season model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->delete()) {
            if(file_exists($model->small_banner)) {
                unlink($model->small_banner);
            }
            if(file_exists($model->large_banner)) {
                unlink($model->large_banner);                
            }
        }
        
        Yii::$app->session->setFlash('success', "Season <strong>$model->title</strong> was deleted succesfully.");
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Season model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Season the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Season::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    /**
     * Finds all season models based on series_id parameter.
     * @param integer $id ($series_id)
     * @return Seasons the loaded models || false if nothing can be found
     */
    public function actionFindAllAsJson()
    {
        if (isset($_POST['depdrop_parents'])) {
            $series = $_POST['depdrop_parents'];
            if ($series != null) {
                $out = Season::find()->select(['id', 'title as name'])->where(['series_id' => $series[0]])->asArray()->all();
                
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
}
