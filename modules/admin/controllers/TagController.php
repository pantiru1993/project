<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use app\modules\admin\models\Tag;
use Yii;

/**
 * TagController implements the actions for Tag model.
 */
class TagController extends Controller
{
    /*
     * Lists all the tags in the database
     * @param $query the query passed by selectize from the view
     * @return JSON
     */
    public function actionList($query)
    {
        $models = Tag::findAllByName($query);
        $items = [];

        foreach ($models as $model) {
            $items[] = ['name' => $model->name];
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $items;
    }
}
