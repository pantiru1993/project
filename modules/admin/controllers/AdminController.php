<?php

namespace app\modules\admin\controllers;

use dektrium\user\controllers\AdminController as BaseAdminController;
use app\modules\admin\models\{Movie, Episode, Season, Series};

/**
 * Default controller for the `admin` module
 */
class AdminController extends BaseAdminController
{
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'ruleConfig' => [
//                    'class' => AccessRule::className(),
//                ],
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['admin'],
//                    ],
//                ],
//            ],
//        ];
//    }
    
    /**
     * Index page of admin controller
     * @return mixed
     */
    public function actionDashboard()
    {
        $views = (new \yii\db\Query())->from('hits')->count();
        $users = \dektrium\user\models\User::find()->count();
        $episodes = Episode::find()->count();
        $seasons = Season::find()->count();
        $series = Series::find()->count();
        $movies = Movie::find()->count();
        
        $recentMovies = Movie::find()->limit(3)->orderBy('created_at')->all();
        $recentSeries = Series::find()->limit(3)->orderBy('created_at')->all();
        $recentEpisodes = Episode::find()->limit(3)->orderBy('created_at')->all();

//        $date = new \DateTime;
//        $timestamp = $date->getTimestamp();        
//        $timestamp-$movie->created_at < 60 * 60 * 24 * 3;
        $newMembers = \dektrium\user\models\User::find()->orderBy('created_at')->limit(8)->all();
        
        //die(var_dump($newMembers));
        return $this->render('dashboard', [
            'views' => $views,
            'users' => $users,
            'episodes' => $episodes,
            'seasons' => $seasons,
            'series' => $series,
            'movies' => $movies,
            'newMembers' => $newMembers,
            'recentMovies' => $recentMovies,
            'recentSeries' => $recentSeries,
            'recentEpisodes' => $recentEpisodes,
        ]);
    }
}
