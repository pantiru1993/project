<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\EpisodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Episodes';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="episode-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Episode', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'attribute' => 'series',
                'label' => 'Series',
                'value' => 'series.title'
            ],
            [
                'attribute' => 'season',
                'label' => 'Season',
                'value' => 'season.title'
            ],
            'title',
            'brief:ntext',
            'slug',
            //'year',
            //'banner',
            //'click',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['view', 'slug' => $model->slug]), ['title' => Yii::t('yii', 'View'),]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['update', 'slug' => $model->slug]), ['title' => Yii::t('yii', 'Update'),]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['delete', 'slug' => $model->slug]), [
                                'title' => Yii::t('yii', 'Delete'),
                                'aria-label' => "Delete",
                                'data-pjax' => "0",
                                'data-confirm' => "Are you sure you want to delete this item?",
                                'data-method' => "post"
                            ]);
                    }
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
