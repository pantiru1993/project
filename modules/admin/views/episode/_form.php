<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Url;
use sjaakp\illustrated\Uploader;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Episode */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="episode-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'series_id')->widget(Select2::classname(), [
        'theme' => 'default',
        'data' => $series,
        'options' => ['placeholder' => 'Select a series...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'season_id')->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'pluginOptions'=>[
            'depends'=>['episode-series_id'],
            'placeholder'=>'Select...',
            'url'=>Url::to(['/admin/season/find-all-as-json'])
        ],
        'select2Options' => [
            'theme' => 'default',
        ]
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'episode_number')->textInput() ?>

    <?= $form->field($model, 'brief')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'year')->textInput() ?>
    
    <?= $form->field($model, 'trailer')->textInput() ?>
    
    <?= $form->field($model, 'server_1')->textInput() ?>
    
    <?= $form->field($model, 'server_2')->textInput() ?>
    
    <?= $form->field($model, 'server_3')->textInput() ?>
    
    <?= $form->field($model, 'imdb_rating')->textInput() ?>
    
    <?= $form->field($model, 'small_banner')->widget(Uploader::class, []) ?>
    
    <?= $form->field($model, 'large_banner')->widget(Uploader::class, []) ?>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
