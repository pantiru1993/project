<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-4 col-xs-6">
            <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-red"><i class="fa fa-eye"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Views</span>
                    <span class="info-box-number"><?=$views?></span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div>
        <div class="col-md-4 col-xs-6">
            <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Users</span>
                    <span class="info-box-number"><?=$users?></span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div>
        <div class="col-md-4 col-xs-6">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-files-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Movies</span>
                    <span class="info-box-number"><?=$movies?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-4 col-xs-6">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-files-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Series</span>
                    <span class="info-box-number"><?=$series?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-4 col-xs-6">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-files-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Seasons</span>
                    <span class="info-box-number"><?=$seasons?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-4 col-xs-6">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-files-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Episodes</span>
                    <span class="info-box-number"><?=$episodes?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Latest Members</h3>

                <div class="box-tools pull-right">
                    <span class="label label-danger">8 New Members</span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <ul class="users-list clearfix">
                    <?php foreach($newMembers as $newMember): ?>
                    <li>
        <!--                <img src="dist/img/user1-128x128.jpg" alt="User Image">-->
                        <a class="users-list-name" href="#"><?=$newMember->username?></a>
                        <span class="users-list-date"><?=\Yii::$app->formatter->asDate($newMember->created_at, 'full')?></span>
                    </li>
                    <?php endforeach; ?>
                    <?php foreach($newMembers as $newMember): ?>
                    <li>
        <!--                <img src="dist/img/user1-128x128.jpg" alt="User Image">-->
                        <a class="users-list-name" href="#"><?=$newMember->username?></a>
                        <span class="users-list-date"><?=\Yii::$app->formatter->asDate($newMember->created_at, 'full')?></span>
                    </li>
                    <?php endforeach; ?>
                </ul>
                <!-- /.users-list -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <a href="<?=Url::to('/user/admin/index')?>" class="uppercase">View All Users</a>
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Recent Movies</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul class="products-list product-list-in-box">
                    <?php foreach($recentMovies as $item): ?> 
                    <li class="item">
                        <div class="product-img">
                            <img src="<?=$item->getImgSrc('small_banner')?>" alt="<?=$item->title?>">
                        </div>
                        <div class="product-info">
                            <a href="<?=Url::to(['movie/view', 'slug' => $item->slug])?>" class="product-title"><?=$item->title?>
                                <span class="label label-info pull-right">movies</span></a>
                            <span class="product-description">
                                <?=$item->brief?>
                            </span>
                        </div>
                    </li>
                    <?php endforeach; ?>
                    <!-- /.item -->
                </ul>
            </div>
            <!-- /.box-body -->
        <!--    <div class="box-footer text-center">
                <a href="javascript:void(0)" class="uppercase">View All Products</a>
            </div>-->
            <!-- /.box-footer -->
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Recent Episodes</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul class="products-list product-list-in-box">
                    <?php foreach($recentEpisodes as $item): ?> 
                    <li class="item">
                        <div class="product-img">
                            <img src="<?=$item->getImgSrc('small_banner')?>" alt="<?=$item->title?>">
                        </div>
                        <div class="product-info">
                            <a href="<?=Url::to(['episode/view', 'slug' => $item->slug])?>" class="product-title"><?=$item->title?>
                                <span class="label label-warning pull-right">episodes</span></a>
                            <span class="product-description">
                                <?=$item->brief?>
                            </span>
                        </div>
                    </li>
                    <?php endforeach; ?>
                    <!-- /.item -->
                </ul>
            </div>
            <!-- /.box-body -->
        <!--    <div class="box-footer text-center">
                <a href="javascript:void(0)" class="uppercase">View All Products</a>
            </div>-->
            <!-- /.box-footer -->
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Recent Series</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul class="products-list product-list-in-box">
                    <?php foreach($recentSeries as $item): ?> 
                    <li class="item">
                        <div class="product-img">
                            <img src="<?=$item->getImgSrc('small_banner')?>" alt="<?=$item->title?>">
                        </div>
                        <div class="product-info">
                            <a href="<?=Url::to(['series/view', 'slug' => $item->slug])?>" class="product-title"><?=$item->title?>
                                <span class="label label-danger pull-right">series</span></a>
                            <span class="product-description">
                                <?=$item->brief?>
                            </span>
                        </div>
                    </li>
                    <?php endforeach; ?>
                    <!-- /.item -->
                </ul>
            </div>
            <!-- /.box-body -->
        <!--    <div class="box-footer text-center">
                <a href="javascript:void(0)" class="uppercase">View All Products</a>
            </div>-->
            <!-- /.box-footer -->
        </div>
    </div>
</div>

