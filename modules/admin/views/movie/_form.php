<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\selectize\SelectizeTextInput;
use sjaakp\illustrated\Uploader;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Movie */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="movie-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'brief')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'year')->textInput() ?>
    
    <?= $form->field($model, 'trailer')->textInput() ?>
    
    <?= $form->field($model, 'server_1')->textInput() ?>
    
    <?= $form->field($model, 'server_2')->textInput() ?>
    
    <?= $form->field($model, 'server_3')->textInput() ?>

    <?= $form->field($model, 'imdb_rating')->textInput() ?>
    
    <?= $form->field($model, 'tagNames')->widget(SelectizeTextInput::className(), [
        // calls an action that returns a JSON object with matched
        // tags
        'loadUrl' => ['tag/list'],
        'options' => ['class' => 'form-control'],
        'clientOptions' => [
            'plugins' => ['remove_button'],
            'valueField' => 'name',
            'labelField' => 'name',
            'searchField' => ['name'],
            'create' => true,
        ],
    ])->hint('Use commas to separate tags') ?>
    
 
    <?= $form->field($model, 'small_banner')->widget(Uploader::class, []) ?>
    
    <?= $form->field($model, 'large_banner')->widget(Uploader::class, []) ?>
 
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
