<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?=Yii::$app->user->identity->username;?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Home', 'icon' => 'home' , 'url' => ['/admin/admin/dashboard']],
                    [
                        'label' => 'Manage users',
                        'icon' => 'user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'View all users', 'icon' => 'users', 'url' => ['/user/admin/index'],],
                            ['label' => 'Create new user', 'icon' => 'user-plus', 'url' => ['/user/admin/create'],],
//                            [
//                                'label' => 'Level One',
//                                'icon' => 'circle-o',
//                                'url' => '#',
//                                'items' => [
//                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
//                                    [
//                                        'label' => 'Level Two',
//                                        'icon' => 'circle-o',
//                                        'url' => '#',
//                                        'items' => [
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
//                                        ],
//                                    ],
//                                ],
//                            ],
                        ],
                    ],
                    ['label' => 'Manage series', 'icon' => 'play', 'url' => ['/admin/series/index']],
                    ['label' => 'Manage seasons', 'icon' => 'play', 'url' => ['/admin/season/index']],
                    ['label' => 'Manage episodes', 'icon' => 'play', 'url' => ['/admin/episode/index']],
                    ['label' => 'Manage movies', 'icon' => 'play', 'url' => ['/admin/movie/index']],
                ],
            ]
        ) ?>

    </section>

</aside>
