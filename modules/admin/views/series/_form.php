<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use sjaakp\illustrated\Uploader;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Series */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="series-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'brief')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'year')->textInput() ?>
    
    <?= $form->field($model, 'trailer')->textInput() ?>
    
    <?= $form->field($model, 'imdb_rating')->textInput() ?>
    
    <?= $form->field($model, 'small_banner')->widget(Uploader::class, []) ?>
    
    <?= $form->field($model, 'large_banner')->widget(Uploader::class, []) ?>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
