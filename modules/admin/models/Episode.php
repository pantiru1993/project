<?php

namespace app\modules\admin\models;

use yii\behaviors\TimestampBehavior;
use app\modules\admin\models\Season;
use app\modules\admin\models\Series;
use yii\behaviors\SluggableBehavior;
use sjaakp\illustrated\Illustrated;

/**
 * This is the model class for table "episode".
 *
 * @property int $id
 * @property int $season_id
 * @property int $episode_number
 * @property string $title
 * @property string $brief
 * @property string $slug
 * @property int $year
 * @property string $trailer
 * @property string $server_1
 * @property string $server_2
 * @property string $server_3
 * @property string $large_banner
 * @property string $small_banner
 * @property decimal $imdb_rating
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Season $season
 */
class Episode extends \yii\db\ActiveRecord
{
    public $series_id;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'episode';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['season_id', 'episode_number','title', 'year', 'imdb_rating',], 'required'],
            [['season_id', 'episode_number','year', 'created_at', 'updated_at'], 'integer'],
            [['imdb_rating'], 'double'],
            [['brief'], 'string'],
            [['title', 'trailer', 'server_1', 'server_2', 'server_3'], 'string', 'max' => 255],
            [['episode_number'], 'unique', 'targetAttribute' => 'season_id'],
            [['season_id'], 'exist', 'skipOnError' => true, 'targetClass' => Season::className(), 'targetAttribute' => ['season_id' => 'id']],
            [['series_id'], 'exist', 'skipOnError' => true, 'targetClass' => Series::className(), 'targetAttribute' => ['series_id' => 'id']],
        ];
    }
    
        /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'hit' => [
                'class' => \usualdesigner\yii2\behavior\HitableBehavior::className(),
                'attribute' => 'hits_count',    //attribute which should contain uniquie hits value
                'group' => false,               //group name of the model (class name by default)
                'delay' => 60 * 60,             //register the same visitor every hour
                'table_name' => '{{%hits}}'     //table with hits data
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => 'true'
            ],
            [
                "class" => Illustrated::class,
                "attributes" => [
                    "small_banner" => [	// attribute name of the illustration   
                        'aspectRatio' => 0.91,
                        'cropSize' => 300,
                    ],                        
                    "large_banner" => [	// attribute name of the illustration
                        'aspectRatio' => 1.8,
                        'cropSize' => 650
                    ],                        
                ],
            ],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'season_id' => 'Season ID',
            'episode_number' => 'Episode number',
            'title' => 'Title',
            'brief' => 'Brief',
            'slug' => 'Slug',
            'year' => 'Year',
            'large_banner' => 'Large Banner',
            'small_banner' => 'Small Banner',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'imdb_rating' => 'IMDB rating'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(Season::className(), ['id' => 'season_id']);
    }
    
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeries()
    {
        return $this->hasOne(Series::className(), ['id' => 'series_id'])->viaTable('season', ['id' => 'season_id']);
    }
}
