<?php

namespace app\modules\admin\models;

use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use sjaakp\illustrated\Illustrated;

/**
 * This is the model class for table "series".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property int $year
 * @property string $trailer
 * @property string $large_banner
 * @property string $small_banner
 * @property int imdb_rating
 * @property int $created_at
 * @property int $updated_at
 */
class Series extends \yii\db\ActiveRecord
{   
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'series';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'year', 'imdb_rating', ], 'required'],
            [['imdb_rating'], 'double'],
            [['brief'], 'string'],
            [['year', 'created_at', 'updated_at'], 'integer'],
            [['title', 'trailer', ], 'string', 'max' => 255],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'hit' => [
                'class' => \usualdesigner\yii2\behavior\HitableBehavior::className(),
                'attribute' => 'hits_count',    //attribute which should contain uniquie hits value
                'group' => false,               //group name of the model (class name by default)
                'delay' => 60 * 60,             //register the same visitor every hour
                'table_name' => '{{%hits}}'     //table with hits data
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => 'true'
            ],
            [
                "class" => Illustrated::class,
                "attributes" => [
                    "small_banner" => [	// attribute name of the illustration   
                        'aspectRatio' => 0.91,
                        'cropSize' => 300,
                    ],                        
                    "large_banner" => [	// attribute name of the illustration
                        'aspectRatio' => 1.8,
                        'cropSize' => 650
                    ],                        
                ],
            ],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'year' => 'Year',
            'large_banner' => 'Large Banner',
            'small_banner' => 'Small Banner',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'imdb_rating' => 'IMDB rating'
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeasons()
    {
        return $this->hasMany(Season::className(), ['series_id' => 'id'])->orderBy(['season_number' => SORT_ASC]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEpisodes()
    {
        return $this->hasMany(Episode::className(), ['season_id' => 'id'])
                ->via('seasons');
    }
}
