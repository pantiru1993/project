<?php

namespace app\modules\admin\models;
 
use app\modules\login\models\User;

/**
 * Signup form
 */
class UpdateForm extends User
{
    public $password;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'unique'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'unique'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['password', 'string', 'min' => 6],
            ['role', 'required']
        ];
    }
    
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function userUpdate()
    { 
        if ($this->validate()) {        
            
            //if the user entered a password then hash it and update it
            $this->password != null ? $this->setPassword($this->password) : null;

            $this->updateRole();
                        
            $this->save(false);
            
            return $this;
        }
        
        return null;
    } 
}