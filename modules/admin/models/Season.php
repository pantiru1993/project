<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "season".
 *
 * @property int $id
 * @property int $series_id
 * @property int $season_number
 * @property string $title
 * @property int $year
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Episode[] $episodes
 */
class Season extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'season';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['series_id', 'season_number','title', 'year'], 'required'],
            [['series_id', 'season_number','year', 'created_at', 'updated_at'], 'integer'],
            [['season_number'], 'unique', 'targetAttribute' => 'series_id'],
            [['title'], 'string', 'max' => 255],
        ];
    }
    
        /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'hit' => [
                'class' => \usualdesigner\yii2\behavior\HitableBehavior::className(),
                'attribute' => 'hits_count',    //attribute which should contain uniquie hits value
                'group' => false,               //group name of the model (class name by default)
                'delay' => 60 * 60,             //register the same visitor every hour
                'table_name' => '{{%hits}}'     //table with hits data
            ],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'series_id' => 'Series ID',
            'season_number' => 'Season number',
            'title' => 'Title',
            'year' => 'Year',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEpisodes()
    {
        return $this->hasMany(Episode::className(), ['season_id' => 'id'])->orderBy(['episode_number' => SORT_ASC]);
    }
    
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeries()
    {
        return $this->hasOne(Series::className(), ['id' => 'series_id']);
    }
}
