<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Season;

/**
 * SeasonSearch represents the model behind the search form of `app\modules\admin\models\Season`.
 */
class SeasonSearch extends Season
{
    public $series;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'series_id', 'year', 'created_at', 'updated_at'], 'integer'],
            [['title', 'series'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Season::find()->joinWith('series');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->setSort([
            'attributes' => [
                'series' => [
                    'asc' => ['series.title' => SORT_ASC],
                    'desc' => ['series.title' => SORT_DESC],
                ],
                'title',
                'year'
            ]
        ]);
        
        //I have made this change because load will use form name: MovieSearch[year]=2014 instead of year=2014
        //$this->load($params);
        if(isset($params['MovieSearch'])) {
            //This is used for back-end where we don't care about pretty URLs + gridview already comes with MovieSearch[year]=2014
            $this->load($params);
        } else {
            //This is used for front-end
            $this->attributes = $params;
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'series_id' => $this->series_id,
            'year' => $this->year,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'series.title', $this->series])
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
