<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Series;

/**
 * SeriesSearch represents the model behind the search form of `app\modules\admin\models\Series`.
 */
class SeriesSearch extends Series
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'year', 'created_at', 'updated_at'], 'integer'],
            [['title', 'slug', 'large_banner', 'small_banner',], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Series::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        //I have made this change because load will use form name: MovieSearch[year]=2014 instead of year=2014
        //$this->load($params);
        if(isset($params['MovieSearch'])) {
            //This is used for back-end where we don't care about pretty URLs + gridview already comes with MovieSearch[year]=2014
            $this->load($params);
        } else {
            //This is used for front-end
            $this->attributes = $params;
        }
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'year' => $this->year,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'large_banner', $this->large_banner])
            ->andFilterWhere(['like', 'small_banner', $this->small_banner]);
        
        $dependency = new \yii\caching\DbDependency(['sql' => 'SELECT MAX(updated_at) FROM series', 'reusable' => true]);
        
        self::getDb()->cache(function ($db) use ($dataProvider) {
            $dataProvider->prepare();
        }, 3600, $dependency);
        
        return $dataProvider;
    }
}
