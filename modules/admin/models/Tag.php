<?php

namespace app\modules\admin\models;


use Yii;
use dosamigos\taggable\Taggable;

/**
 * This is the model class for table "tag".
 *
 * @property int $id
 * @property string $name
 * @property int $frequency
 */
class Tag extends \yii\db\ActiveRecord
{
    
    /**
    * @var string helper attribute to work with tags
    */
    public $tagNames;
        
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['frequency'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['tagNames'], 'safe'],
        ];
    }
    
    /**
    * @inheritdoc
    */
   public function behaviors()
   {
       return [
           Taggable::className(),
       ];
   }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'frequency' => 'Frequency',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMovies()
    {
        return $this->hasMany(Movie::className(), ['id' => 'movie_id'])->viaTable('movie_tag_assn', ['tag_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('movie_tag_assn', ['movie_id' => 'id']);
    }
    
    /**
     * @param string $name
     * @return Tag[]
     */
    public static function findAllByName($name)
    {
        return static::find()
            ->where(['like', 'name', $name])->limit(50)->all();
    }
}
