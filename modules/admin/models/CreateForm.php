<?php

namespace app\modules\admin\models;
 
use Yii;
use app\modules\login\models\SignupForm;
use yii\helpers\ArrayHelper;
use app\modules\login\models\User;

/**
 * Signup form
 */
class CreateForm extends SignupForm
{
 
    public $role;
 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules = ArrayHelper::merge(
                        $rules, [
                    ['role', 'required']
        ]);
        return $rules;
        
    }
 
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function create()
    { 
        if ($this->validate()) {
 
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;                      
            $user->setPassword($this->password);
            $user->generateAuthKey();        
            $user->save(false);

            $user->assignRole($this->role);

            return $user;
        }
        
        return null;
    } 

}