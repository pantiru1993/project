<?php

use yii\helpers\Url;
use yii\jui\Accordion;

$this->title = $model->title;

if($model instanceof app\modules\admin\models\Episode) {
    $this->params['breadcrumbs'][] = ['label' => 'Seriale', 'url' => url::to('/project/series/view-all')];
    $this->params['breadcrumbs'][] = ['label' => $model->series->title, 'url' => url::to(['/project/series/view' , 'id' => $model->series->id])];
    $this->params['breadcrumbs'][] = ['label' => $model->season->title];
} else if ($model instanceof app\modules\admin\models\Series) {
    $this->params['breadcrumbs'][] = ['label' => 'Seriale', 'url' => url::to('/project/series/view-all')];
} else if ($model instanceof \app\modules\admin\models\Movie) {
    $this->params['breadcrumbs'][] = ['label' => 'Filme', 'url' => url::to('/project/movie/view-all')];
}

$this->params['breadcrumbs'][] = $this->title;
?>


<div class="col-md-8 left-content">
<!--        <div class="inner-part-header">
        <h3 class="inner-title"><?= $model->title?></h3>
    </div>-->
    <div class="large-banner">
        <?=$model->getImgHtml('large_banner')?>
    </div>
    
    <div class="description-container">
        <h4 class="padding-bottom-2">Descriere</h4>
        <div class="description">
            <?=$model->brief;?>
        </div>
    </div>
    <?php if(isset($model->server_1) && $model->server_1):?>
    <div class="video-wrapper">
        <h4 class="padding-bottom-2">Server 1</h4>
        <div class="video-container">
            <iframe allowfullscreen="true" frameborder="0" 
                    height="280" mozallowfullscreen="true" scrolling="no" 
                    src="<?=$model->server_1?>" webkitallowfullscreen="true" 
                    width="100%" style="">                    
            </iframe>
        </div>
    </div>
    <?php endif;?>
    <?php if(isset($model->server_2) && $model->server_2):?>
    <div class="video-wrapper">
        <h4 class="padding-bottom-2">Server 2</h4>
        <div class="video-container">
            <iframe allowfullscreen="true" frameborder="0" 
                    height="280" mozallowfullscreen="true" scrolling="no" 
                    src="<?=$model->server_2?>" webkitallowfullscreen="true" 
                    width="100%" style="">                    
            </iframe>
        </div>
    </div>
    <?php endif;?>
    <?php if(isset($model->server_3) && $model->server_3):?>
    <div class="video-wrapper">
        <h4 class="padding-bottom-3">Server 3</h4>
        <div class="video-container">
            <iframe allowfullscreen="true" frameborder="0" 
                    height="280" mozallowfullscreen="true" scrolling="no" 
                    src="<?=$model->server_3?>" webkitallowfullscreen="true" 
                    width="100%" style="">                    
            </iframe>
        </div>
    </div>
    <?php endif;?>

    <!--<hr>-->
    
</div>
<div class="col-md-4 right-content">
    <?php if(isset($seasons) && !empty($seasons)): ?>
    <h4 class="side-title">Navigare rapida</h4>
    <div id="slinky-menu" class="slinky-menu">
        <ul>
            <?php foreach ($seasons as $season): ?>
            <li>
                <a href="#" class="next"><?=$season->title?></a>
                <ul <?= isset($model->season->id) ? ($season->id == $model->season->id ? 'class="active"' : '') : ''?>>
                    <?php if($season->episodes): ?>
                        <?php foreach($season->episodes as $episode): ?>
                        <li <?= isset($model->season->id) ? ($model->id == $episode->id ? 'class="active-episode"' : '') : ''?>>
                            <a href="<?=Url::to(['episode/view', 'slug' => $episode->slug])?>"><?=$episode->episode_number . ' - ' . $episode->title?></a>
                        </li>
                        <?php endforeach;?>
                    <?php else: ?>
                        <li class="no-episode">
                            There are no episodes added yet.
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php endif; ?>

    <?= $this->render('_right', [
        'model' => $model,
        'views' => $views,
        'related' => $related
    ]) ?>
    <div class="clearfix"> </div>
</div>
<div class="clearfix"></div>



<!--<div class="single-agile-shar-buttons">
    <h5 >Share This :</h5>
    <ul>
        <li>
            <div class="fb-like" data-href="https://www.facebook.com/w3layouts" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
            <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
        </li>
        <li>
            <div class="fb-share-button" data-href="https://www.facebook.com/w3layouts" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2Fw3layouts&amp;src=sdkpreparse">Share</a></div>
        </li>
        <li class="news-twitter">
            <a href="https://twitter.com/w3layouts" class="twitter-follow-button" data-show-count="false">Follow @w3layouts</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
        </li>
        <li>
            <a href="https://twitter.com/intent/tweet?screen_name=w3layouts" class="twitter-mention-button" data-show-count="false">Tweet to @w3layouts</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
        </li>
        <li>
             Place this tag where you want the +1 button to render. 
            <div class="g-plusone" data-size="medium"></div>

             Place this tag after the last +1 button tag. 
            <script type="text/javascript">
                (function () {
                    var po = document.createElement('script');
                    po.type = 'text/javascript';
                    po.async = true;
                    po.src = 'https://apis.google.com/js/platform.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(po, s);
                })();
            </script>
        </li>
    </ul>
</div>-->