<?php

use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use toriphes\lazyload\LazyLoad;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\MovieSearch / app\modules\admin\models\SeriesSearch  */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cautare';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="col-md-10">
        <?php Pjax::begin(); ?>
        <div class="row actions-toolbar">

            <?php $totalCount = $dataProvider->getTotalCount();?>
            <?php if ($totalCount): ?> 
            <span class="total-count hidden-xs"><?=$totalCount?> <?= $totalCount > 1 ? 'rezultate gasite.' : 'rezultat gasit.'?></span>
            <span class="pull-right">Ordoneaza: <?=$dataProvider->sort->link('year', ['label' => 'An']) . " | " . 
                $dataProvider->sort->link('created_at', ['label' => 'Cele mai recente']);?></span>

        </div>
        <div class="results-wrapper">
                <div class="row">
                <?php foreach($dataProvider->models as $model): ?>
                    <div class="col-xs-4 col-sm-4 col-md-3 item-card">
                        <a href="<?=Url::to(['view', 'slug' => $model->slug])?>"><?=LazyLoad::widget(['src' => $model->getImgSrc('small_banner'), 'options' => ['class' => 'img-responsive', 'title' => $model->title]])?>
                            <div class="play-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                        </a>
                        <div class="item-description">
                            <div class="item-description-upper">
                                <h6><a class="item-title" href="<?=Url::to(['view', 'slug' => $model->slug])?>"><?=$model->title?>	</a></h6>						
                            </div>
                            <div class="item-description-bottom">
                                <p><?=$model->year?></p>
                                <div class="block-stars">
                                    <i class="fa fa-star" aria-hidden="true"></i> <?=$model->imdb_rating?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <?php 
                            $date = new DateTime;
                            $timestamp = $date->getTimestamp();
                        ?>
                        <?php if($timestamp-$model->created_at < 60 * 60 * 24 * 3): ?>
                        <div class="ribbon">
                            <p>NOU</p>
                        </div>
                        <?php endif;?>
                    </div>
                <?php endforeach; ?>
                </div>

                <div class="row actions-footbar">
                    <div class="col-xs-6 results-count">
                    <?= $dataProvider->getCount()?> <?= $dataProvider->getCount() > 1 ? 'rezultate pe pagina.' : 'rezultat pe pagina.'?>
                    </div>
                    <div class="pagination-wrapper col-xs-6">
                        <?= LinkPager::widget([
                            'pagination' => $dataProvider->pagination
                        ]);?>
                    </div>
                </div>
            <?php else: ?>
                    <h3>Nici un rezultat gasit.</h3>
            <?php endif;?>                
        </div>
        <?php Pjax::end(); ?>
    </div>
</div>
