<?php
    use yii\helpers\Url;
?>


<div class="right">
    <h4 class="side-title">Trailer<span></span></h3>
    <div class="video-container">
        <iframe width="100%" height="315" src="<?=$model->trailer?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
    
    <div class="player-text side-bar-info">
        <div class="row description-items">
            <div class="col-xs-4">
                <div class="item-name">
                    An<label class="pull-right">:</label>
                </div>
            </div>
            <div class="col-xs-8">
                <div class="item-brief">
                    <a href="<?=Url::to(['movie/view-all', 'MovieSearch[year]' => $model->year])?>"><?=$model->year?></a>
                </div>
            </div>
        </div>
        <?php if($model instanceof \app\modules\admin\models\Movie): ?>
        <div class="row description-items">
            <div class="col-xs-4">
                <div class="item-name">
                    Gen<label class="pull-right">:</label>
                </div>
            </div>
            <div class="col-xs-8">
                <div class="item-brief">                    
                    <?php foreach($model->tags as $tag): ?>
                    <a href="<?=Url::to(['movie/view-all', 'MovieSearch[tag]' => $tag->name])?>"><?=$tag->name?></a>
                    <?php endforeach; ?>                    
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="row description-items">
            <div class="col-xs-4">
                <div class="item-name">
                    Nota IMDB<label class="pull-right">:</label>
                </div>
            </div>
            <div class="col-xs-8">
                <div class="item-brief">
<!--                    <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>-->
                    <?=$model->imdb_rating?>
                </div>
            </div>
        </div>
        <div class="row description-items">
            <div class="col-xs-4">
                <div class="item-name">
                    Vizualizari<label class="pull-right">:</label>
                </div>
            </div>
            <div class="col-xs-8">
                <div class="item-brief">
                    <?=$views?>
                </div>
            </div>
        </div>
    </div>
    
</div>


  
<div class="right">
    <h4 class="side-title">Recomandari<span></span></h4>
    <div class="row">
        <?php foreach ($related as $model): ?>
        <div class="item-card col-xs-6 col-sm-4 col-md-12">
            <a href="<?=Url::to(['view', 'slug' => $model->slug])?>"><img src="<?=$model->getImgSrc('small_banner')?>" title="<?=$model->title?>" class="img-responsive" alt=" ">
                <div class="item-title-abs"><?=$model->title?></div>
            </a>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="clearfix"> </div>
</div>

</div>
<!--            <h4 class="side-title">For Latest <span>Movies</span></h4>
<div class="side-bar-form">
    <form action="#" method="post">
        <input type="search" name="email" placeholder="Search here...." required="required">
        <input type="submit" value=" ">
    </form>
</div>
<h4 class="side-title">Hot <span>Topics</span></h3>
<ul class="side-bar-agile">
    <li><a href="single.html">John Abraham, Sonakshi Sinha and Tahir ...</a><p>Sep 29, 2016</p></li>
    <li><a href="single.html">Romantic drama about two people finding out that love</a><p>Feb 3, 2016</p></li>
    <li><a href="single.html">Storks have moved on from delivering babies to packages ...</a><p>Aug 1, 2016</p></li>
    <li><a href="single.html">John Abraham, Sonakshi Sinha and Tahir ...</a><p>Sep 29, 2016</p></li>
    <li><a href="single.html">John Abraham, Sonakshi Sinha and Tahir ...</a><p>Sep 29, 2016</p></li>
</ul>-->