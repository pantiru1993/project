<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\FrontendAsset;
use yii\helpers\Url;
use kartik\typeahead\Typeahead;
use yii\web\JsExpression;

FrontendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!--/main-header-->
<!--/banner-section-->
<section id="upper-section" data-zs-src='["/images/2.jpg", "/images/1.jpg", "/images/3.jpg","/images/4.jpg"]' data-zs-overlay="dots" data-zs-bullets="false">
    <div class="inner-content">
        <!--/header-w3l-->
        <div class="header" id="home">
            <div class="container">
                <div class="inner-header">	
                    <nav class="navbar navbar-default">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <h1><a  href="<?=Url::home()?>"><span>C</span>lub <span>S</span>eriale</a></h1>
                        </div>
                        <!-- navbar-header -->
                        <div class="collapse navbar-collapse" id="navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="<?=Url::home()?>">Home</a></li>
<!--                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Categorie <b class="caret"></b></a>
                                    <ul class="dropdown-menu multi-column columns-3">
                                        <li>
                                            <div class="col-sm-4">
                                                <ul class="multi-column-dropdown">
                                                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'actiune'])?>">Actiune</a></li>
                                                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'biografie'])?>">Biografie</a></li>
                                                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'crima'])?>">Crima</a></li>
                                                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'familie'])?>">Familie</a></li>
                                                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'horror'])?>">Horror</a></li>
                                                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'romantic'])?>">Romantic</a></li>
                                                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'sport'])?>">Sport</a></li>
                                                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'razboi'])?>">Razboi</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-4">
                                                <ul class="multi-column-dropdown">
                                                    <li><a href="genre.html">Adventure</a></li>
                                                    <li><a href="comedy.html">Comedy</a></li>
                                                    <li><a href="genre.html">Documentary</a></li>
                                                    <li><a href="genre.html">Fantasy</a></li>
                                                    <li><a href="genre.html">Thriller</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-4">
                                                <ul class="multi-column-dropdown">
                                                    <li><a href="genre.html">Animation</a></li>
                                                    <li><a href="genre.html">Costume</a></li>
                                                    <li><a href="genre.html">Drama</a></li>
                                                    <li><a href="genre.html">History</a></li>
                                                    <li><a href="genre.html">Musical</a></li>
                                                    <li><a href="genre.html">Psychological</a></li>
                                                </ul>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                    </ul>
                                </li>-->
                                <li><a href="<?=Url::to(['/project/series/view-all'])?>">Seriale</a></li>
                                <li><a href="<?=Url::to(['/project/movie/view-all'])?>">Filme</a></li>
                            </ul>

                        </div>
                        <div class="account-dropdown">
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-user-circle"></i> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
<!--                                        <li><a href="#" class="login"  data-toggle="modal" data-target="#modal-1">Login</a></li>
                                    <li><a href="#" class="login reg"  data-toggle="modal" data-target="#modal-2">Register</a></li>-->
                                    <?php if(Yii::$app->user->isGuest): ?>
                                        <li><a href="<?=Url::to('/conectare')?>">Intra in cont</a></li>   
                                        <li><a href="<?=Url::to('/inregistrare')?>">Inregistreaza-te</a></li>                                           
                                    <?php else: ?>
                                        <li><a href="<?=Url::to('/cont')?>">Contul meu</a></li>
                                        <li><a href="<?=Url::to('/profil')?>">Profil</a></li>
                                        <hr>
                                        <li><a href="<?=Url::to('/deconectare')?>" data-method="post">Deconectare</a></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"> </div>	
                    </nav>
                    <div class="search-wrapper">
                        <div class="cd-main-header">
                            <ul class="cd-header-buttons">
                                <li><a class="cd-search-trigger" href="#cd-search"> <span></span></a></li>
                            </ul>
                            
                        </div>
                        <div id="cd-search" class="cd-search">
                            <form action="#" method="post">
                                <?php 
                                $movieTemplate = '<div><a href="/filme/{{slug}}" class="search-item-title"><img class="search-item-banner" src=/illustrations/movie/small_banner/{{small_banner}}>{{value}} - {{year}}</a>';
                                $episodeTemplate = '<div><a href="/episoade/{{slug}}" class="search-item-title"><img class="search-item-banner" src=/illustrations/episode/small_banner/{{small_banner}}>{{value}} - {{year}}</a>';
                                $seriesTemplate = '<div><a href="/seriale/{{slug}}" class="search-item-title"><img class="search-item-banner" src=/illustrations/series/small_banner/{{small_banner}}>{{value}} - {{year}}</a>';
                                echo Typeahead::widget([
                                    'name' => 'twitter_oss', 
                                    'options' => ['placeholder' => 'Cauta...'],
                                    'dataset' => [
                                        [
                                            'prefetch' => '/project/episode/json-episode-list',
                                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                            'displayKey' => 'value',
                                            'templates' => [
                                                'suggestion' => new JsExpression("Handlebars.compile('{$episodeTemplate}')"),
                                                'header' => '<h3 class="search-items-category">Episoade</h3>'
                                            ]
                                        ],
                                        [
                                            'prefetch' => '/project/movie/json-movie-list',
                                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                            'displayKey' => 'value',
                                            'templates' => [
                                                'suggestion' => new JsExpression("Handlebars.compile('{$movieTemplate}')"),
                                                'header' => '<h3 class="search-items-category">Filme</h3>'
                                            ]
                                        ],
                                        [
                                            'prefetch' => '/project/series/json-series-list',
                                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                            'displayKey' => 'value',
                                            'templates' => [
                                                'suggestion' => new JsExpression("Handlebars.compile('{$seriesTemplate}')"),
                                                'header' => '<h3 class="search-items-category">Seriale</h3>'
                                            ]
                                        ],
                                    ]
                                ]);
                                ?>
                            </form>
                        </div>
                    </div>

                </div> 
            </div> <!-- end of container-->

        </div>
        <!--//header-w3l-->
        <!--/banner-info-->
        <div class="banner-info">
            <?php if (isset($this->title)): ?>
                <h3><?=$this->title?></h3>
            <?php else: ?>
                <h3><span>C</span>ele <span>m</span>ai <span>n</span>oi <span>f</span>ilme</h3>
            <?php endif;?>            
        </div>
        <!--/banner-ingo-->		
    </div>
</section>
<!--/banner-section-->
<!--//main-header-->
<!-- Modal1 -->
<!--<div class="modal fade" id="modal-1" tabindex="-1" role="dialog" >

    <div class="modal-dialog">
         Modal content
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>Login</h4>
                <div class="login-form">
                    <form action="#" method="post">
                        <input type="email" name="email" placeholder="E-mail" required="">
                        <input type="password" name="password" placeholder="Password" required="">
                        <div class="tp">
                            <input type="submit" value="LOGIN NOW">
                        </div>
                        <div class="forgot-grid">
                            <div class="log-check">
                                <label class="checkbox"><input type="checkbox" name="checkbox">Remember me</label>
                            </div>
                            <div class="forgot">
                                <a href="#" data-toggle="modal" data-target="#myModal2">Forgot Password?</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
<!-- //Modal1 -->
<!-- Modal1 -->
<!--<div class="modal fade" id="modal-2" tabindex="-1" role="dialog" >

    <div class="modal-dialog">
         Modal content
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>Register</h4>
                <div class="login-form">
                    <form action="#" method="post">
                        <input type="text" name="name" placeholder="Name" required="">
                        <input type="email" name="email" placeholder="E-mail" required="">
                        <input type="password" name="password" placeholder="Password" required="">
                        <input type="password" name="conform password" placeholder="Confirm Password" required="">
                        <div class="signin-rit">
                            <span class="agree-checkbox">
                                <label class="checkbox"><input type="checkbox" name="checkbox">I agree to your <a class="w3layouts-t" href="#" target="_blank">Terms of Use</a> and <a class="w3layouts-t" href="#" target="_blank">Privacy Policy</a></label>
                            </span>
                        </div>
                        <div class="tp">
                            <input type="submit" value="REGISTER NOW">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
<!-- //Modal1 -->
<!--/content-inner-section-->
<div class="content-inner">
    <div class="breadcrumb-container">
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
    </div>
    <div class="container">
        <div class="main-wrapper">
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>
<!--//content-inner-section-->

<?=$this->render('_footer'); ?>

<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

<?php if(isset(Yii::$app->user->identity->isAdmin) && Yii::$app->user->identity->isAdmin): ?>
<a href="<?=Url::to('/dashboard')?>" class="area-jumper btn btn-danger" >GO TO ADMIN</a>
<?php endif;?>

<!--<div id="small-dialog1" class="mfp-hide">
    <iframe src="https://player.vimeo.com/video/165197924?color=ffffff&title=0&byline=0&portrait=0"></iframe>
</div>
<div id="small-dialog2" class="mfp-hide">
    <iframe src="https://player.vimeo.com/video/165197924?color=ffffff&title=0&byline=0&portrait=0"></iframe>
</div>-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
