<?php
use yii\helpers\Url;
?>

<!--/footer-bottom-->
<div class="footer-wrapper" id="contact">
    <div class="footer-inner container">
<!--        <h2>Sign up for our <span>Newsletter</span></h2>
        <p class="para">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus 
            tristique bibendum. Donec rutrum sed sem quis venenatis.</p>
        <div class="footer-contact">
            <form action="#" method="post">
                <input type="email" name="Email" placeholder="Enter your email...." required=" ">
                <input type="submit" value="Subscribe">
            </form>
        </div>-->
        <div class="footer-grids">
            <div class="col-md-3 footer-grid">
                <h4>Informatii utile</h4>
                <ul>
                    <li><a href="<?=Url::to(['/project/default/contact'])?>">Contact</a></li>
                    <li><a href="<?=Url::to(['/project/default/dmca'])?>">DMCA</a></li>
                </ul>
            </div>
            
            <div class="col-md-3 footer-grid">
                <h4>Seriale dupa an</h4>
                <ul> 
                    <?php $year = date('Y'); ?>
                    <?php for($i=$year; $i>$year-5; $i--): ?>
                        <li><a href="<?=Url::to(['series/view-all', 'year' => $i])?>" title="<?=$i?>"><?=$i?></a></li> 
                    <?php endfor;?>                    
                </ul>
            </div>
            
            <div class="col-md-3 footer-grid">
                <h4>Social</h4>
                <ul class="social-icons">
                    <li><a href="https://www.facebook.com/groups/264783510996692/?ref=br_rs" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCjJTY71cWogsT9FIsYBpd5w" target="_blank"><i class="fab fa-youtube"></i></a></li>
                </ul>
            </div>
<!--            <div class="col-md-2 footer-grid">
                <h4>Movies by category</h4>
                <ul class="tags">
                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'action'])?>">ACTION</a></li>
                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'fantasy'])?>">FANTASY</a></li>
                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'comedy'])?>">COMEDY</a></li>
                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'horror'])?>">HORROR</a></li>
                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'romance'])?>">ROMANCE</a></li>
                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'action'])?>">ACTION</a></li>
                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'fantasy'])?>">FANTASY</a></li>
                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'comedy'])?>">COMEDY</a></li>
                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'horror'])?>">HORROR</a></li>
                    <li><a href="<?=Url::to(['movie/view-all', 'tag' => 'romance'])?>">ROMANCE</a></li>
                </ul>
            </div>-->

<!--            <div class="col-md-3 footer-grid hidden-xs">
                <h4>Latest Movies</h4>
                <?php $recent_movies = \app\modules\admin\models\Movie::find()->orderBy(['created_at' => SORT_DESC])->limit(4)->all();?>
                <?php foreach($recent_movies as $movie): ?>
                <div class="footer-item">
                    
                    <div class="footer-item-left">
                        <a href="<?=Url::to(['view', 'slug' => $movie->slug]) ?>"><img src="<?=$movie->getImgSrc('large_banner')?>" alt="<?=$movie->title?>" class="img-responsive"></a>
                    </div>
                    <div class="footer-item-right">
                        <a href="<?=Url::to(['view', 'slug' => $movie->slug]) ?>"><?= $movie->title ?></a>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <?php endforeach; ?>
            </div>-->

            <div class="col-md-3 footer-grid">
                <h4 class="b-log"><a href="index.html"><span>C</span>lub <span>S</span>eriale</a></h4>
                <?php $recent_series = \app\modules\admin\models\Series::find()->orderBy(['created_at' => SORT_DESC])->limit(6)->all();?>
                <?php foreach($recent_series as $model): ?>
                    <div class="footer-grid-instagram">
                        <a href="<?=Url::to(['/project/series/view', 'slug' => $model->slug]) ?>"><img src="<?=$model->getImgSrc('large_banner')?>" alt="<?=$movie->title?>" alt=" " class="img-responsive"></a>
                    </div>
                <?php endforeach; ?>
                

                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
<!--            <ul class="bottom-links">
                <li><a href="icons.html" title="Font Icons">Icons</a></li> 
                <li><a href="short-codes.html" title="Short Codes">Short Codes</a></li> 
                <li><a href="contact.html" title="contact">Contact</a></li> 

            </ul>-->
        </div>
<!--        <h3 class="text-center follow">Connect <span>Us</span></h3>
        <ul class="social-icons">
            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
            <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
            <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
        </ul>	-->

    </div>

</div>
<div class="footer-copyright">
    <p>© <?=date('Y'); ?> Club Seriale. Toate drepturile rezervate</p>
</div>