<?php

use yii\helpers\Url;
use app\assets\IndexAsset;
use toriphes\lazyload\LazyLoad;

IndexAsset::register($this);
?>

<!--<div class="tabs-wrapper">
    /tab-section
    <div id="horizontal-tab">
        <ul class="resp-tabs-list">
            <li>Cele mai recente seriale</li>
            <li>Cele mai recente episoade</li>
            <li>Top episoade</li>
        </ul>
        <div class="resp-tabs-container">
            <div class="tab-1">
                <div class="tab-wrapper">
                    <div class="tab-wrapper-content">
                        <div class="col-md-5 video-player">
                            <div class="video-content">
                                <div data-video="f2Z65fobH2I" id="video"> <img src="images/11.jpg" alt="" class="img-responsive" /> </div>
                            </div>
                            <div class="player-text">
                                <p class="item-header">Force 2</p>
                                <div class="row description-items">
                                    <div class="col-xs-3">
                                        <div class="item-name">
                                            Story Line:
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="item-brief">
                                            <?= '$model->brief' ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row description-items">
                                    <div class="col-xs-3">
                                        <div class="item-name">
                                            Released on:
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="item-brief">
                                            <?= '$model->year' ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row description-items">
                                    <div class="col-xs-3">
                                        <div class="item-name">
                                            Genres:
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="item-brief">
                                            <a href="genre.html">Drama</a> | 
                                            <a href="genre.html">Adventure</a> | 
                                            <a href="genre.html">Family</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row description-items">
                                    <div class="col-xs-3">
                                        <div class="item-name">
                                            Star Rating:
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="item-brief">
                                            <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 items-list">
                            <?php foreach ($recent_movies as $model): ?>
                                <div class="col-xs-4 col-sm-4 col-md-3 item-card">
                                    <a href="<?=Url::to(['view', 'slug' => $model->slug])?>"><?=LazyLoad::widget(['src' => $model->getImgSrc('small_banner'), 'options' => ['class' => 'img-responsive', 'title' => $model->title]])?>
                                        <div class="play-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                                    </a>
                                    <div class="item-description">
                                        <div class="item-description-upper">
                                            <h6><a class="item-title" href="<?=Url::to(['view', 'slug' => $model->slug])?>"><?=$model->title?>	</a></h6>						
                                        </div>
                                        <div class="item-description-bottom">
                                            <p><?=$model->year?></p>
                                            <div class="block-stars">
                                                <i class="fa fa-star" aria-hidden="true"></i> 7.5
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <?php 
                                        $date = new DateTime;
                                        $timestamp = $date->getTimestamp();
                                    ?>
                                    <?php if($timestamp-$model->created_at < 60 * 60 * 24 * 3): ?>
                                    <div class="ribbon">
                                        <p>NEW</p>
                                    </div>
                                    <?php endif;?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"></div>
                </div>	
            </div>
            <div class="tab-2">
                <div class="tab-wrapper">
                    <div class="tab-wrapper-content">
                        <div class="col-md-5 video-player">
                            <div class="video-content">
                                <div data-video="f2Z65fobH2I" id="video"> <img src="images/11.jpg" alt="" class="img-responsive" /> </div>
                            </div>
                            <div class="player-text">
                                <p class="item-header">Force 2</p>
                                <div class="row description-items">
                                    <div class="col-xs-3">
                                        <div class="item-name">
                                            Story Line:
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="item-brief">
                                            <?= '$model->brief' ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row description-items">
                                    <div class="col-xs-3">
                                        <div class="item-name">
                                            Released on:
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="item-brief">
                                            <?= '$model->year' ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row description-items">
                                    <div class="col-xs-3">
                                        <div class="item-name">
                                            Genres:
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="item-brief">
                                            <a href="genre.html">Drama</a> | 
                                            <a href="genre.html">Adventure</a> | 
                                            <a href="genre.html">Family</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row description-items">
                                    <div class="col-xs-3">
                                        <div class="item-name">
                                            Star Rating:
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="item-brief">
                                            <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-7 items-list">
                            <?php foreach ($recent_movies as $movie): ?>
                                <div class="col-xs-4 item-card">
                                    <a href="<?= Url::to(['movie/view', 'slug' => $movie->slug]) ?>"><?= LazyLoad::widget(['src' => $movie->getImgSrc('small_banner'), 'options' => ['class' => 'img-responsive', 'title' => $movie->title]]) ?>
                                        <div class="play-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                                    </a>
                                    <div class="item-description">
                                        <div class="item-description-upper">
                                            <h6><a class="item-title" href="<?= Url::to(['/project/movie/view', 'slug' => $movie->slug]) ?>"><?= $movie->title ?>	</a></h6>						
                                        </div>
                                        <div class="item-description-bottom">
                                            <p><?= $movie->year ?></p>
                                            <div class="block-stars">
                                                <i class="fa fa-star" aria-hidden="true"></i> <?= $movie->imdb_rating ?>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <?php
                                    $date = new DateTime;
                                    $timestamp = $date->getTimestamp();
                                    ?>
                                    <?php if ($timestamp - $movie->created_at < 60 * 60 * 24 * 3): ?>
                                        <div class="ribbon">
                                            <p>NOU</p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="tab-3">
                <div class="tab-wrapper">
                    <div class="tab-wrapper-content">
                        <div class="col-md-5 video-player">
                                                                <div class="video-content">
                                                            <div data-video="BXEZFd0RT5Y" id="video2"> <img src="images/44.jpg" alt="" class="img-responsive" /> </div>
                                                        </div>

                            <div class="player-text">
                                <p class="item-header">Storks </p>
                                <p class="item-para"><span class="conjuring_w3">Story Line<label>:</label></span>Starring: Andy Samberg, Jennifer Aniston, Ty Burrell Storks Official Trailer 3 (2016) - Andy Samberg Movie the company's top delivery stork, lands in hot water when the Baby Factory produces an adorable....... </p>
                                <p class="item-para"><span>Release On<label>:</label></span>Aug 1, 2016 </p>
                                <p class="item-para">
                                    <span>Genres<label>:</label> </span>
                                    <a href="genre.html">Drama</a> | 
                                    <a href="genre.html">Adventure</a> | 
                                    <a href="genre.html">Family</a>								
                                </p>
                                <p class="item-para fexi_header_para1"><span>Star Rating<label>:</label></span>
                                    <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                </p>
                            </div>

                        </div>
                        <div class="col-md-7 items-list">
                            
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"></div>
                </div>	
            </div>
        </div>
    </div>

</div>-->
<!--//tab-section-->	

<!--seriale-->
<div class="section-wrapper">
    <h3 class="section-title">Seriale recente</h3>
    <div class="owl-carousel owl-theme">
        <?php foreach ($recent_series as $model): ?>
            <div class="item">
                <div class="item-card">
                    <a href="<?= Url::to(['movie/view', 'slug' => $model->slug]) ?>"><?= LazyLoad::widget(['src' => $model->getImgSrc('small_banner'), 'options' => ['class' => 'img-responsive', 'title' => $model->title]]) ?>
                        <div class="play-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                    </a>
                    <div class="item-description">
                        <div class="item-description-upper">
                            <h6><a class="item-title" href="single.html"><?= $model->title ?></a></h6>							
                        </div>
                        <div class="item-description-bottom">
                            <p><?= $model->year ?></p>
                            <div class="block-stars">
                                <i class="fa fa-star" aria-hidden="true"></i> <?=$model->imdb_rating?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php
                    $date = new DateTime;
                    $timestamp = $date->getTimestamp();
                    ?>
                    <?php if ($timestamp - $model->created_at < 60 * 60 * 24 * 3): ?>
                        <div class="ribbon">
                            <p>NOU</p>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<!--//seriale-->

<!--episoade-->
<div class="section-wrapper">
    <h3 class="section-title">Episoade recente</h3>
    <div class="owl-carousel owl-theme">
        <?php foreach ($recent_episodes as $model): ?>
            <div class="item">
                <div class="item-card">
                    <a href="<?= Url::to(['movie/view', 'slug' => $model->slug]) ?>"><?= LazyLoad::widget(['src' => $model->getImgSrc('small_banner'), 'options' => ['class' => 'img-responsive', 'title' => $model->title]]) ?>
                        <div class="play-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                    </a>
                    <div class="item-description">
                        <div class="item-description-upper">
                            <h6><a class="item-title" href="single.html"><?= $model->title ?></a></h6>							
                        </div>
                        <div class="item-description-bottom">
                            <p><?= $model->year ?></p>
                            <div class="block-stars">
                                <i class="fa fa-star" aria-hidden="true"></i> <?=$model->imdb_rating?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php
                    $date = new DateTime;
                    $timestamp = $date->getTimestamp();
                    ?>
                    <?php if ($timestamp - $model->created_at < 60 * 60 * 24 * 3): ?>
                        <div class="ribbon">
                            <p>NOU</p>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<!--//episoade-->

<!--movies-->
<div class="section-wrapper">
    <h3 class="section-title">Filme recente</h3>
    <div class="owl-carousel owl-theme">
        <?php foreach ($recent_movies as $model): ?>
            <div class="item">
                <div class="item-card">
                    <a href="<?= Url::to(['movie/view', 'slug' => $model->slug]) ?>"><?= LazyLoad::widget(['src' => $model->getImgSrc('small_banner'), 'options' => ['class' => 'img-responsive', 'title' => $model->title]]) ?>
                        <div class="play-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                    </a>
                    <div class="item-description">
                        <div class="item-description-upper">
                            <h6><a class="item-title" href="single.html"><?= $model->title ?></a></h6>							
                        </div>
                        <div class="item-description-bottom">
                            <p><?= $model->year ?></p>
                            <div class="block-stars">
                                <i class="fa fa-star" aria-hidden="true"></i> <?=$model->imdb_rating?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php
                    $date = new DateTime;
                    $timestamp = $date->getTimestamp();
                    ?>
                    <?php if ($timestamp - $model->created_at < 60 * 60 * 24 * 3): ?>
                        <div class="ribbon">
                            <p>NOU</p>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<!--//movies-->

<!--other slider-->
<div class="section-wrapper">
    <?php
        //echo
    //    \edofre\sliderpro\SliderPro::widget([
    //        'id' => 'my-slider',
    //        'sliderOptions' => [
    //            'width' => 960,
    //            'height' => 500,
    //            'arrows' => false,
    //            'buttons' => true,
    //            'thumbnailArrows' => true,
    //            'init' => new \yii\web\JsExpression("
    //			function() {
    //				//console.log('slider is initialized');
    //			}
    //		"),
    //        ],
    //    ]);
        ?>

    <!--    <div class="slider-pro" id="my-slider">
            <div class="sp-slides">
                 Slide 2 
                <div class="sp-slide">
                    <img class="sp-image" src="images/11"/>
                    <p class="sp-caption">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                 Slide 3 
                <div class="sp-slide">
                    <img class="sp-image" src="images/11"/>
                    <h3 class="sp-layer sp-black"
                        data-position="bottomLeft" data-horizontal="10%"
                        data-show-transition="left" data-show-delay="300" data-hide-transition="right">
                        Lorem ipsum dolor sit amet
                    </h3>
                    <p class="sp-layer sp-white sp-padding"
                       data-width="200" data-horizontal="center" data-vertical="40%"
                       data-show-transition="down" data-hide-transition="up">
                        consectetur adipisicing elit
                    </p>
                    <div class="sp-layer sp-static">Static content</div>
                    <p class="sp-caption">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                 Slide 4 
                <div class="sp-slide">
                    <img class="sp-image" src="images/11"/>
                    <h3 class="sp-layer sp-black"
                        data-position="bottomLeft" data-horizontal="10%"
                        data-show-transition="left" data-show-delay="300" data-hide-transition="right">
                        Lorem ipsum dolor sit amet
                    </h3>
                    <p class="sp-layer sp-white sp-padding"
                       data-width="200" data-horizontal="center" data-vertical="40%"
                       data-show-transition="down" data-hide-transition="up">
                        consectetur adipisicing elit
                    </p>
                    <div class="sp-layer sp-static">Static content</div>
                </div>
                 Slide 5 
                <div class="sp-slide">
                    <img class="sp-image" src="images/11"/>
                    <h3 class="sp-layer sp-black"
                        data-position="bottomLeft" data-horizontal="10%"
                        data-show-transition="left" data-show-delay="300" data-hide-transition="right">
                        Lorem ipsum dolor sit amet
                    </h3>
                    <p class="sp-layer sp-white sp-padding"
                       data-width="200" data-horizontal="center" data-vertical="40%"
                       data-show-transition="down" data-hide-transition="up">
                        consectetur adipisicing elit
                    </p>
                    <div class="sp-layer sp-static">Static content</div>
                </div>
    
                 thumbnails 
                <div class="sp-thumbnails">
                    <img class="sp-thumbnail" src="images/11" data-src="images/11"/>
                    <img class="sp-thumbnail" src="images/11" data-src="images/11"/>
                    <img class="sp-thumbnail" src="images/11" data-src="images/11"/>
                    <p class="sp-thumbnail">Thumbnail 4</p>
                    <p class="sp-thumbnail">Thumbnail 5</p>
                </div>
            </div>
        </div>-->
</div>
<!--//other slider-->