<?php
$this->title = 'DMCA policy';
$this->params['breadcrumbs'][] = $this->title;
?>
<h3 class="section-title">DMCA POLICY</h3>
<div>
    <p>
        club-seriale.online website does not host,store or upload any video file.
        We have no knowledge of whether content shown on third party websites is or is not authorized
        by the content owner as that is a matter between the host site and the content owner. 
        club-seriale.online does not host any Content on its servers or network. 
        club-seriale.online is just a complex system of automatic indexers,robotic scripts and user submissions.
    </p>
    <br>
    <p>
        Content owners can use the DMCA protocol to request removal of links to videos that they believe to 
        infringe their copyright,but they have to understand that by having a link removed from club-seriale.online they will not be removing the actual 
        source video from the third party website. 
        Content owners must contact the video hosting site(open platforms as youtube,dailymotion,vimeo,megavideo,vk) themselves to request the actual source link removal.
    </p> 
    <br>
    <p>If you are an intellectual property owner or an agent thereof and believe that either any Content on club-seriale.online website or 
        any material or activity contained on an online location to which club-seriale.online has referred or linked users, infringes upon
        your intellectual property rights, you may submit a notification pursuant to the Digital Millennium Copyright Act (“DMCA”) by providing 
        our Agent for Notice of Claims with the following information in writing (see 17 U.S.C. 512(c)(3) and 512(d) for further detail):
    </p>
    <p>
        1. electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;
    </p>
    <p>
        2. identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works on the Website are covered by a single notification,
        a representative list of such works on the Website;
    </p>
    <p>
        3. identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is 
        to be removed or access to which is to be disabled, and information reasonably sufficient to permit us to locate the material 
        (or in the case of referrals or links that are claimed to lead to infringing material or activity, identification of the reference or link that is 
        to be removed or access to which is to be disabled, and information reasonably sufficient to permit us to locate that reference or link);
    </p>
    <p>
        4. information reasonably sufficient to permit us to contact you, such as an address, telephone number, and, if available, an e-mail address;
    </p>
    <p>
        5. a statement that you have a good faith belief that use of the material in the manner complained of is not 
        authorized by the copyright owner, its agent, or the law; and
    </p>
    <p>
        6. a statement that the information in the notification is accurate, and under penalty of perjury, 
        that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.
    </p>
    <br>
    <p>
        You acknowledge that if you fail to comply with all of the requirements of this section, your DMCA notice 
        may not be valid. E-mails sent to club-seriale.online without a proper subject line,without the requested informations
        or for purposes other than communication about intellectual property claims, may not be acknowledged or responded to.
    </p>
    <br>
    <p>
        All copyright infringement claims should be emailed to club-seriale.online `s designated agent : contact@club-seriale.online
    </p>
</div>