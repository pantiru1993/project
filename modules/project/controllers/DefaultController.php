<?php

namespace app\modules\project\controllers;

use yii\web\Controller;
use app\modules\admin\models\{Movie,Series,Episode};

/**
 * Default controller for the `project` module
 */
class DefaultController extends Controller
{   
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    /**
     * Renders the index view for the module
     * @return mixed
     */
    public function actionIndex()
    {
        $movieDependency = new \yii\caching\DbDependency(['sql' => 'SELECT MAX(updated_at) FROM movie', 'reusable' => true]);
        $seriesDependency = new \yii\caching\DbDependency(['sql' => 'SELECT MAX(updated_at) FROM series', 'reusable' => true]);
        $episodeDependency = new \yii\caching\DbDependency(['sql' => 'SELECT MAX(updated_at) FROM episode', 'reusable' => true]);
               
        return $this->render('index', [
            'recent_movies' => Movie::find()->cache(3600, $movieDependency)->orderBy('created_at DESC')->limit(6)->all(),
            'recent_series' => Series::find()->cache(3600, $seriesDependency)->orderBy('created_at DESC')->limit(6)->all(),
            'recent_episodes' => Episode::find()->cache(3600, $episodeDependency)->orderBy('created_at DESC')->limit(6)->all(),
            
            //'popular_movies' => Movie::find()->cache(3600, $dependency)->orderBy('hits_count DESC')->limit(6)->all(),
        ]);
    }
    
    /*
     * Renders the DMCA page
     * @return mixed
     */
    public function actionDmca()
    {
        return $this->render('dmca');
    }
    /*
     * Renders the contact page
     * @return mixed
     */
    public function actionContact()
    {
        return $this->render('contact');
    }
}
