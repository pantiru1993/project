<?php

namespace app\modules\project\controllers;

use Yii;
use yii\filters\AccessControl;
use dektrium\user\filters\AccessRule;
use app\modules\admin\models\Series;

/**
 * MovieController implements the CRUD actions for Movie model.
 */
class SeriesController extends \app\modules\admin\controllers\SeriesController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view', 'view-all', 'json-series-list'],
                    ],
                ],
            ],
        ];
    }
    
    /**
     * Displays a single Movie model.
     * @param integer $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($slug)
    {
        $model = $this->findModelBySlug($slug);
        $model->getBehavior('hit')->touch();
        $views = $model->getBehavior('hit')->getHitsCount();
        $related = Series::find()->where(['<>','id', $model->id])->orderBy(new \yii\db\Expression('rand()'))->limit(5)->all();

        return $this->render('/common/view', [
            'model' => $model,
            'seasons' => $model->seasons,
            'views' => $views,
            'related' => $related
        ]);
    }
    
        /**
     * Lists all Movie models.
     * @return mixed
     */
    public function actionViewAll()
    {
        $searchModel = new \app\modules\admin\models\SeriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/common/viewAll', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /** 
    * Returns an array of movies as JSON
     * @return json
    */
    public function actionJsonSeriesList($q = null) {
        $query = new \yii\db\Query;

        $query->select('title, year, id, small_banner, slug')
            ->from('series')
            ->where('title LIKE "%' . $q .'%"')
            ->orderBy('title');
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out = [];
        foreach ($data as $d) {
            $out[] = ['value' => $d['title'], 'year' => $d['year'], 'id' => $d['id'], 'small_banner' => $d['small_banner'], 'slug' => $d['slug']];
        }
        echo \yii\helpers\Json::encode($out);
    }
}