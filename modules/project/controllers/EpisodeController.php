<?php

namespace app\modules\project\controllers;

use app\modules\admin\models\Season;
use yii\filters\AccessControl;
use dektrium\user\filters\AccessRule;
use app\modules\admin\models\Series;

/**
 * EpisodeController extends the main Movie controller.
 */
class EpisodeController extends \app\modules\admin\controllers\EpisodeController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view', 'view-all', 'json-episode-list'],
                    ],
                ],
            ],
        ];
    }
    
    /**
     * Displays a single Episode model.
     * @param integer $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($slug)
    {
        $model = $this->findModelBySlug($slug);
        $seasons = Season::find()->where(['series_id' => $model->season->series->id])->orderBy(['season_number' => SORT_ASC])->all();
        
        $model->getBehavior('hit')->touch();
        $views = $model->getBehavior('hit')->getHitsCount();
        
        $related = Series::find()->where(['<>','id', $seasons[0]->series_id])->orderBy(new \yii\db\Expression('rand()'))->limit(5)->all();

        return $this->render('/common/view', [
            'model' => $model,
            'seasons' => $seasons,
            'views' => $views,
            'related' => $related
        ]);
    }
    
    /** 
    * Returns an array of episodes as JSON
     * @return json
    */
   public function actionJsonEpisodeList($q = null) {
       $query = new \yii\db\Query;

       $query->select('title, year, id, small_banner, slug')
           ->from('episode')
           ->where('title LIKE "%' . $q .'%"')
           ->orderBy('title');
       $command = $query->createCommand();
       $data = $command->queryAll();
       $out = [];
       foreach ($data as $d) {
           $out[] = ['value' => $d['title'], 'year' => $d['year'], 'id' => $d['id'], 'small_banner' => $d['small_banner'], 'slug' => $d['slug']];
       }
       echo \yii\helpers\Json::encode($out);
   }

}