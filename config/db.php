<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=another',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

//    'enableSchemaCache'=>!YII_DEBUG,
//    'schemaCacheDuration'=>3600,
//    'schemaCache'=>'cache',
//
//    'enableQueryCache'=>!YII_DEBUG,
//    'queryCacheDuration'=>3600,
];
