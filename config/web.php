<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'language' => 'ro',
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'name' => 'Project',
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'X6Gqw8XinhncKGcWxJ73eJbuNgNAE7mm',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'dektrium\user\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/conectare'],
        ],
        'errorHandler' => [
            'errorAction' => '/project/default/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
  
                /*FRONT-END*/
                'conectare' => 'user/security/login',
                'inregistrare' => 'user/registration/register',
                'cont' => 'user/settings/account',
                'profil' => 'user/settings/profile',
                'deconectare' => 'user/security/logout',
                'dmca' => 'project/default/dmca',
                'contact' => 'project/default/contact',
                'seriale' => 'project/series/view-all',
                //'<controller>s' => 'project/<controller>/view-all',
                'seriale/<slug>' => 'project/series/view',
                //'<controller>s/<slug>' => 'project/<controller>/view',
                'filme' => 'project/movie/view-all',
                'filme/<slug>' => 'project/movie/view',
                'episoade/<slug>' => 'project/episode/view',
                
                /*FRONT-END*/                
                
                /*BACK-END*/
                'dashboard' => 'admin/admin/dashboard',
                '<module>/<controller>/<action>/<slug>' => '<module>/<controller>/<action>',
                /*BACK-END*/
            ],
        ],
    ],
    'params' => $params,
    'modules' => [
        'project' => [
            'class' => 'app\modules\project\ProjectModule',
            //here is the magic for allowing to see a module
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    
                ]
            ]
        ],
        'admin' => [
            'class' => 'app\modules\admin\AdminModule',
            //here is the magic for allowing to see a module
            'as access' => [
                'class' => \yii\filters\AccessControl::className(),
                'ruleConfig' => [
                    'class' => \dektrium\user\filters\AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            //here is the magic for allowing to see a module
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'confirm', 'error', 'resend', 'request', 'register', 'reset'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'denyCallback' => function ($rule, $action) { 
                            Yii::$app->session->setFlash('danger', "You need to login in order to access this page.");
                            return $this->redirect('user/security/login');
                        }
                    ]
                ]
            ],
            'enableFlashMessages' => false,
            'enableAccountDelete' => true,
            'admins' => ['admin'],
            'controllerMap' => [
                'admin' => [
                    'class'  => 'app\modules\admin\controllers\AdminController',
                    'layout' => '@app/modules/admin/views/layouts/main.php',
                ],
                'registration' => [
                    'class' => \dektrium\user\controllers\RegistrationController::className(),
                    'layout' => '@app/common/views/layouts/authentication.php',
                    'on ' . \dektrium\user\controllers\RegistrationController::EVENT_AFTER_REGISTER => function ($e) {
                        Yii::$app->response->redirect(['/user/security/login'])->send();
                        Yii::$app->end();
                    }
                ],
                'security' => [
                    'class' => \dektrium\user\controllers\SecurityController::className(),
                    'layout' => '@app/common/views/layouts/authentication.php',
                    'on ' . \dektrium\user\controllers\SecurityController::EVENT_AFTER_LOGIN => function ($e) {
                        //check if user is admin and redirect to admin panel
                        Yii::$app->user->identity->isAdmin ?
                            Yii::$app->response->redirect(['dashboard'])->send() :
                            //else redirect user to the app
                            Yii::$app->response->redirect(['/'])->send();
                        Yii::$app->end();
                    }
                ],
                'recovery' => [
                    'class' => \dektrium\user\controllers\RecoveryController::className(),
                    'layout' => '@app/common/views/layouts/authentication.php',
                ],
                'settings' => [
                    'class' => 'app\common\controllers\SettingsController',
                ],
            ],
        ],
    ],
    'defaultRoute' => 'project/default/index',
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'],  
        'generators' => [ //here
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'adminlte' => '@vendor/dmstr/yii2-adminlte-asset/gii/templates/crud/simple',
                ]
            ]
        ],
    ];
}

return $config;
